import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firstdemo/util/ApiClient.dart';
import 'package:firstdemo/util/CS.dart';
import 'package:firstdemo/util/CU.dart';
import 'package:firstdemo/util/Enum.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

Map<String, dynamic> resjson = <String, dynamic>{};
DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
PackageInfo packageInfo;

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    return Theme(
        data: new ThemeData(backgroundColor: Colors.green),
        child: Scaffold(
          body: Stack(
            children: <Widget>[
              Container(
                color: Colors.black,
                alignment: Alignment.center,
                child: Image.asset(
                  "assets/bg.png",
                  fit: BoxFit.fill,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                ),
              ),
              Align(
                child: Image.asset(
                  "assets/logo.png",
                  height: 105,
                ),
              )
            ],
          ),
        ));
  }

  @override
  void initState() {
    super.initState();
    IsloginUser();
  }

  Future<void> IsloginUser() async {
    var UserInfo = await CU.getUserInfo();
    log("+++++++++++++++++++++++++++++++++");
    log(UserInfo.toString());
    Future.delayed(Duration(seconds: 2), () => {callService()});
  }

  Future<void> callService() async {
    String Token = "";
//    final FirebaseMessaging _fcm = FirebaseMessaging();
//    await _fcm.getToken().then((String token) {
//      Token = token;
//      log("+token+");
//      log(token);
//    });
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String AppVersion = packageInfo.version;
//    Map<String, dynamic> deviceData = await CU.getDeviceState();
    Map<String, dynamic> body = <String, dynamic>{
//      CS.serviceName: CS.PlayScreen,
//      CS.token: CU.DefaultToken,
//      CS.deviceType: Platform.isAndroid ? "1" : "2",
//      CS.deviceId: deviceData[CS.deviceId],
//      CS.deviceVersion: deviceData[CS.deviceVersion],
//      CS.latitude: "0",
//      CS.longitude: "0",
//      CS.deviceToken: Token,
//      CS.deviceName: deviceData[CS.deviceName],
//      CS.appVersion: AppVersion,
    };

    Map<String, dynamic> resJson;
    if (await CU.CheckInternet()) {
      resJson = await ApiClient.Call(
          context: context, apiUrl: CS.playScreen, isShowPogressDilog: false);
    } else {
      CU.showNoInternetDiloag(context, callService);
      return;
    }
    if (resJson[CS.status].toString() == StatusCode.Success) {
      if (resJson != null) {
        Map<String, dynamic> data;
        if (Platform.isIOS) {
          data = resJson[CS.data][CS.ios];
        } else {
          data = resJson[CS.data][CS.android];
        }
        log(data.toString());

        if (AppVersion != data[CS.appVersion] && data[CS.isUpdate] == 1) {
          await CU.showUpdateDiloag(
              context, data[CS.message], data[CS.isComplusory]);
        }

        if (data[CS.is_maintenance] == 1) {
          await CU.showMaintenanceDiloag(
              context, data[CS.maintenance_message], callService);
          return;
        }

        var UserInfo = await CU.getUserInfo();
        if ((UserInfo == null || CU.IsEmptyOrNull(UserInfo[CS.lms_user_id]))) {
//          Future.delayed(
//              Duration(seconds: 2),
//              () => {
//                    Navigator.pushReplacement(
//                        context,
//                        MaterialPageRoute(
//                            builder: (BuildContext context) => LoginScreen()))
//                  });
        } else {
//          Future.delayed(
//              Duration(seconds: 2),
//              () => {
//                    Navigator.of(context).pushAndRemoveUntil(
//                        MaterialPageRoute(builder: (context) => MainScreen()),
//                        (Route<dynamic> route) => false)
//                  });
        }
      }
    } else if (resJson[CS.status].toString() == StatusCode.Error) {
      showDialog(
          barrierDismissible: false,
          context: context,
          child: CU.showDiloag(context, resJson[CS.message]));
      return;
    }
  }
}
